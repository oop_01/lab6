package com.manita.week6;

public class BookBankApp {
    public static void main(String[] args) {
        BookBank manita = new BookBank("Manita",100.0);
        manita.print();
        manita.deposit(50);
        manita.print();
        manita.withdraw(50);
        manita.print();

        BookBank prayood = new BookBank("Prayood",1);
        prayood.print();
        prayood.deposit(1000000);
        prayood.print();
        prayood.withdraw(10000000);

        BookBank praweet = new BookBank("Praweet",10);
        praweet.print();
        praweet.deposit(10000000);
        praweet.withdraw(1000000);
        praweet.print();
    }
}
